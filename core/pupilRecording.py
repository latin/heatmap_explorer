import sys, os

__cam = os.path.split( os.path.abspath(__file__) )[0] + "/util"
sys.path.append(__cam)

from recordingBase         import *
from read_pupil_record     import *
from world_eye_correlation import *
import cPickle as pickle
import os, cv2
import logging
import numpy as np
from reference_surface import Reference_Surface

camera_calibration = {'dist_coefs': np.array([[ 0.06330768, -0.17328079,  0.00074967,  0.000353  ,  0.07648477]]),
                      'camera_name': 'Logitech Webcam C930e',
                      'resolution': (1280, 720),
                      'camera_matrix': np.array([[ 739.72227378,    0.        ,  624.44490772],
                                                  [   0.        ,  717.84832227,  350.46000651],
                                                  [   0.        ,    0.        ,    1.        ]])
                      }

class PupilRecording(RecordingBase):
    def __init__(self, recordPath = "", width = None, height = None, windowSize = 1, maxWindowSize = 10):
        super(PupilRecording, self).__init__()
        self.recordPath = recordPath
        self.width      = width
        self.height     = height
        self.targetWidth  = width
        self.targetHeight = height
        self.maxWindowSize = maxWindowSize
        self.setWindowSize(windowSize)
        self.ready = False
        if self.recordPath != "":
            self._loadRecording()
          
    def setWindowSize(self, windowSize):
        '''Sets a new window size.'''
        self.windowSize = windowSize
        self.sliceSize  = windowSize / self.maxWindowSize
                
    def setMaxWindowSize(self, maxWindowSize):
        '''Sets a new max window size.'''
        self.maxWindowSize = maxWindowSize
        self.sliceSize  = self.windowSize / self.maxWindowSize

    def getFramesCount(self):
        '''Returns the number of frames.'''
        return self.framesCount

    def getFrame(self, index):
        '''Returns the frame in position index'''
        if index >= 0 and index < self.framesCount:
            return self.frames[index]
        return None

    def getCurrentFrame(self):
        '''Returns the current frame index'''
        return self.frames[self.currentIndex]
                
    def getCurrentIndex(self):
        '''Returns the current index.'''
        return self.currentIndex

    def getFramesWindow(self):
        '''Returns a list of frames, from currentIndex-windowSize to currentIndex. 
        Automatically handles extreme case (i.e when currentIndex-windowSize < 0).'''
        i0 = max(0, self.currentIndex - self.windowSize)
        i1 = self.currentIndex
        return self.getIntervalFrames(i0, i1)

    def getGazeWindow(self):
        '''Returns a list of gaze, from currentIndex-windowSize to currentIndex. 
        Automatically handles extreme case (i.e when currentIndex-windowSize < 0).'''
        i0 = max(0, self.currentIndex - self.windowSize)
        i1 = self.currentIndex
        return self.getIntervalGaze(i0, i1)

    def getIntervalFrames(self, i0, i1):
        '''Returns a sequence of frames between 2 indices: i0 and i1.'''
        if not (i0 >= 0 and i1 > i0 and i1 < self.framesCount):
            return []
        if i1-i0+1 < self.maxWindowSize:
            return self.frames[i0:i1]
        sliceSize = int( (i1-i0+1)/self.maxWindowSize )
        if sliceSize < self.sliceSize:
            return self.frames[i1-self.maxWindowSize:i1]
        indices = list(np.arange(i0+sliceSize, i1, sliceSize))
        indices.append(i1)
        indices 
        return [self.frames[i] for i in indices]

    def getIntervalGaze(self, i0, i1):
        '''Returns a sequence of gazes between 2 indices: i0 and i1.'''
        if i0 < 0 or i1 <= i0 or i1 >= self.framesCount:
            return []
        return self.gazeList[i0:i1]

    def getCurrentGaze(self):
        '''Return gaze position corresponding to the current video frame.'''
        return self.gazeList[self.currentIndex]

    def getCurrentTimestamp(self):
        '''Returns timestamp corresponding to the current video frame.'''
        return self.worldTst[self.currentIndex]

    def nextFrame(self):
        '''Moves the frame index to the next frame, returns the new index if succeeded, -1 otherwhise.'''
        if self.currentIndex-1 < self.framesCount:
            self.currentIndex += 1
            return self.currentIndex
        return -1

    def move2index(self, index):
        '''Moves current frame index to the given position.'''
        if index >= 0 and index < self.framesCount:
            self.currentIndex = index
            return index
        return -1

    def _loadRecording(self):
        '''Loads data recorded with the pupil eye tracker'''
        try:
            self.worldVideo = cv2.VideoCapture(self.recordPath + '/world.mp4')
            self.width  = int(self.worldVideo.get(cv2.cv.CV_CAP_PROP_FRAME_WIDTH))
            self.height = int(self.worldVideo.get(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT))
            if self.targetWidth is None:
                self.targetWidth = self.width
            if self.targetHeight is None:
                self.targetHeight = self.height
            self.gaze     = load_object(self.recordPath + '/pupil_data')['gaze_positions']
            self.worldTst = np.load(self.recordPath + '/world_timestamps.npy')
            self.markers  = load_object(self.recordPath + '/square_marker_cache')['marker_cache']
            surface_definitions = load_object(self.recordPath + '/surface_definitions')
            surfaces = [Reference_Surface(saved_definition=d) for d in
                        surface_definitions.get('realtime_square_marker_surfaces', []) if isinstance(d, dict)]

            self.surface = None
            for s in surfaces:
                if s.name == 'screen':
                    self.surface = s
                    break
            if self.surface is None:
                raise Exception("Missing screen surface")
            print "Pupil recording loaded!"
        except:
            print "Error occurred while loading Pupil recording files"
            return 1
        self.ready = True
        gazeTst = [g['timestamp'] for g in self.gaze]
        # Compute correlation between gaze and word
        self.corr = correlate_eye_world(gazeTst, self.worldTst)
        self._findTargetShape()
        self._loadVideoFile()

    def _matchPoints4homography(self, markers):
        # find markers and associate each one with a point on the screen
        # then compute the homography and reconstruct the monitor image
        pass

    def ref_img_to_surface(self, pos, m_to_screen):
        shape = pos.shape
        pos.shape = (-1,1,2)
        new_pos = cv2.perspectiveTransform(pos,m_to_screen )
        new_pos.shape = shape
        return new_pos


    def findCorners(self, markers, min_marker_perimeter=100):
        self.surface.locate(markers, camera_calibration, min_marker_perimeter)
        corners = self.surface.corners_robust
        if len(corners) == 0:
            return None
        corners[:, 1] = 1 - corners[:, 1]
        return corners.dot(np.array([self.width, 0, 0, self.height]).reshape((2, 2))).astype(np.float32)


    def getSurfaceImg(self, frame, markers):
        corners = self.findCorners(markers)
        if corners is None:
            return cv2.resize(frame, (self.targetWidth, self.targetHeight))
        rectified = np.array(((0, self.targetHeight), (self.targetWidth, self.targetHeight), (self.targetWidth, 0), (0, 0)), dtype=np.float32)
        self.m_to_screen = cv2.getPerspectiveTransform(rectified, corners)
        self.m_from_screen = cv2.getPerspectiveTransform(corners, rectified)
        return cv2.warpPerspective(frame, self.m_from_screen, (self.targetWidth, self.targetHeight))


    def _findTargetShape(self):
        width = []
        height = []
        for marker in self.markers:
            corners = self.findCorners(marker)
            if corners is None:
                continue
            for i in range(4):
                edge = np.linalg.norm(corners[i] - corners[(i+1)%4])
                if i % 2:
                    height.append(edge)
                else:
                    width.append(edge)
        # We have trouble converting from numpy.array to QImage if dimensions are not multiple of 32
        if width:
            self.targetWidth = int(round(np.median(width) / 32.)) * 32
        if height:
            self.targetHeight = int(round(np.median(height) / 32.)) * 32


    def _loadVideoFile(self):
        self.frames   = []
        self.gazeList = []
        count = 0
        print "Loading video into memory"
        #cv2.namedWindow("original", cv2.WINDOW_NORMAL)
        #cv2.namedWindow("cropped", cv2.WINDOW_NORMAL)
        ret, frame = self.worldVideo.read()
        while ret:
            frame = cv2.resize(frame, (self.width, self.height))
            newFrame = self.getSurfaceImg(frame, self.markers[count])
            """
            cv2.imshow("original", frame)
            cv2.imshow("cropped", newFrame)
            cv2.waitKey(0)
            """
            self.frames.append(newFrame)
            frameTst = self.worldTst[count]
            gazeIdx  = self.corr[count]
            gazeTst  = self.gaze[gazeIdx]['timestamp']
            if 'realtime gaze on screen' in self.gaze[gazeIdx].keys():
                gazeNormPos = self.gaze[gazeIdx]['realtime gaze on screen']
                gazeX = gazeNormPos[0] * self.targetWidth
                gazeY = (1.0 - gazeNormPos[1]) * self.targetHeight
            else:
                gazePos = np.array(self.gaze[gazeIdx]['norm_pos'])
                gazePos[0] *= frame.shape[1]
                gazePos[1] = (1 - gazePos[1]) * frame.shape[0]
                gazePos = gazePos.reshape(-1,1,2)
                gazePos = cv2.perspectiveTransform(gazePos, self.m_from_screen)[0, 0]
                gazeX, gazeY = gazePos[:2]
            gazeX, gazeY = int(gazeX), int(gazeY)
            self.gazeList.append([gazeTst, gazeX, gazeY])
            ret, frame = self.worldVideo.read()
            count += 1
        self.framesCount = count
        print "Video loaded: %i frames"%count
        self.currentIndex = 0

# ==================================================
def playWithGaze(pr):
    cv2.namedWindow("Player", cv2.WINDOW_NORMAL)
    backupIndex = pr.getCurrentIndex()
    pr.move2index(0)
    while pr.getCurrentIndex() < pr.getFramesCount():
        #pr.getFramesWindow()

        frame = pr.getCurrentFrame()
        gazeX, gazeY = pr.getCurrentGaze()[-2:]
        cv2.circle(frame, (gazeX, gazeY), 5, (255, 0, 0), -1)
        # print frameTst, gazeTst, abs(frameTst - gazeTst), gazeNormPos
        cv2.imshow("Player", frame)
        key = cv2.waitKey(17)
        if key == 27:
            break
        pr.nextFrame()
    pr.move2index(backupIndex)


if __name__ == '__main__':
    recPath = "../../pupil_recordings/gui"
    if len(sys.argv) > 1:
        recPath = sys.argv[1]
    pr = PupilRecording(recordPath = recPath, windowSize = 30)
    print pr.getCurrentIndex()
    playWithGaze(pr)
    print pr.getCurrentIndex()

