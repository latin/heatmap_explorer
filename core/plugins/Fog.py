#!/usr/bin/env python
# -*- coding: utf-8 -*- 

from scipy import *
import math, cv2, numpy as np

class Fog(object):
    def __init__(self, width = 640, height = 480, sigma = 5., fogIntensity = 0.5, alphaImg = 0.5):
        self.width   = width
        self.height  = height
        self.min     = 0.0
        self.cumWeights = np.ndarray((height, width), dtype=np.float)
        self.bleImg  = np.ndarray((height, width, 3), dtype=np.uint8)
        self.m            = max(width, height)*2
        self.x       = arange(0, self.m, 1, float)
        self.y       = self.x[:, newaxis]
        self.setSigma(sigma)
        # Creating blending layer with the specified color
        self.blendingLayer = np.ndarray((height, width, 3), dtype=np.uint8)
        self.setFogIntensity(fogIntensity)
        self.alphaImg = alphaImg
        self.cache = {}
    # -------------------------------------------------

    def setFogIntensity(self, fogIntensity):
        self.fogIntensity = fogIntensity
        self.blendingLayer[:,:,0].fill(int(255.0 * self.fogIntensity))
        self.blendingLayer[:,:,1].fill(int(255.0 * self.fogIntensity))
        self.blendingLayer[:,:,2].fill(int(255.0 * self.fogIntensity))

    def setSigma(self, sigma):
        self.sigma = sigma
        self.center = self.m/2
        self.gauss  = 4*exp(- ( ((self.x-self.center)**2) / (2*self.sigma**2) + ((self.y-self.center)**2) / (2*self.sigma**2 )) )

    def processImage(self, image, gazeList):
        imgSize = image.shape
        # Validate image size: should be the same as defined in the init method
        if imgSize[0] != self.height:
            raise ValueError('Image height is different from width internal value')
        if imgSize[1] != self.width:
            raise ValueError('Image width is different from width internal value')
        # Weight coefficients so so recent samples have higher coefficient
        length = len(gazeList)
        self.coeffs    = np.array( [math.pow(x, 2) for x in range(2, length+2, 1)] ).reshape((length, 1))
        #self.coeffs   /= np.sum(self.coeffs)
        self.coeffs   /= self.coeffs[length-1]
        # Compute sum of gaussian matrix
        self.computeWeights(gazeList)
        # Create the fog
        self.createBlendingLayer(image)
        self.blend(image)
        return self.bleImg
    
    # -------------------------------------------------

    def blend(self, img):
        for k in range(3):
            backgChannel = (img[:,:,k]*self.alphaImg + self.blendingLayer[:,:,k] * (1-self.alphaImg)) * (1-self.cumWeightsMask)
            imageChannel  = self.cumWeightsMask * img[:,:,k]
            #blendChannel  = (1.0 - self.cumWeights)     * self.blendingLayer[:,:,k]
            self.bleImg[:,:,k] = backgChannel + imageChannel# + (1.0 - self.alphaBlend)*imageChannel + self.alphaBlend*blendChannel
        return self.bleImg
    
    # -------------------------------------------------

    def createBlendingLayer(self, img):
        pass # Already created in the init method    
    # -------------------------------------------------
    
    def computeWeights(self, gazeList):
        # Compute gaussian sum
        self.cumWeights.fill(self.min) 
        for i, item in enumerate(gazeList):
            wei = None
            tstamp = None
            if len(item) == 2:
                xx, yy = item[0], item[1]
            else: # assume [tstamp, x, y, ...]
                tstamp, xx, yy = item[0], item[1], item[2]
            if tstamp is not None and tstamp in self.cache.keys():
                wei = self.cache[tstamp]
            if xx >= 0 and xx < self.width and yy >= 0 and yy < self.height:
                bigX = self.center - xx
                bigY = self.center - yy
                wei  = self.gauss[ bigY:(bigY + self.height) , bigX:(bigX + self.width) ]
                if tstamp is not None:
                    self.cache[tstamp] = wei
                # print self.cumWeights.shape, xx, yy, ":", wei.shape
            if wei is not None:
                self.cumWeights += wei * self.coeffs[i]
        self.cumWeights = np.clip( self.cumWeights, 0.0, 1.0 )
        """
        minn = np.amin(self.cumWeights)
        maxx = np.amax(self.cumWeights)
        self.cumWeights = (self.cumWeights - minn) / (maxx - minn)
        """
        self.cumWeightsMask = (self.cumWeights>0.2)
        self.cumWeights *= self.cumWeightsMask


