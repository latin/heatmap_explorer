import csv, cv2, Fog

class FogTest(object):
    '''Class to represent a heatmap. It receives an image and a file containing raw gaze data. Gaza data
    should be a .csv file where each line has the format "tstamp,leftX,leftY,leftPupil,rightX,rightY,rightPupil".'''
    def __init__(self, image, gazeFileName, maxGazeX = 1680, maxGazeY = 1050):
        self.image = image
        self.height, self.width, self.deph = image.shape
        self.maxGazeX, self.maxGazeY = maxGazeX, maxGazeY
        self.gazeFileName = gazeFileName
        # Factor to adjust gaze coordinates to the image size
        self.adjustX = 1.0*self.width / self.maxGazeX
        self.adjustY = 1.0*self.height/ self.maxGazeY

    def _loadGazeInterval(self, t0, t1):
        '''Loads all gaze samples from the file in [t0, t1]. Passing t1 = -1 will load all gaze samples.'''
        self.t0 = t0; self.t1 = t1
        try:
            gazeFile = open(self.gazeFileName, "rt")
        except:
            print "Gaze file %s does not exists"%self.gazeFileName
            return 1
        gazeFile = csv.reader(gazeFile, delimiter=",")
        self.gazeList = []
        print self.gazeList
        for line in gazeFile:
            t = int(line[0])
            if t1 == -1 or (t >= t0 and t <= t1):
                eyeX = ( int(line[1]) + int(line[4]) )/2
                eyeY = ( int(line[2]) + int(line[5]) )/2
                # Scaling coordinates
                eyeX = int(eyeX * self.adjustX)
                eyeY = int(eyeY * self.adjustY)
                self.gazeList.append([t, eyeX, eyeY])

    def _computeFog(self, t0, t1):
        '''Computes the fog with gaze data in [t0.t1].'''
        self._loadGazeInterval(t0, t1) # Load gaze samples in the interval defined by the parameters
        # Now create an object com compute the weight matrix
        self.blender = Fog.Fog(self.width, self.height, sigma = 5.0, alphaImg = 1.0, alphaBlend = 0.7, color = (127, 127, 127))
        # update weights
        self.blender.processImage(self.image, self.gazeList)
        self.fogImage = self.blender.bleImg
        cv2.imwrite("output.jpg", self.fogImage)
                
        
if __name__ == "__main__":
    
    h = FogTest(cv2.imread("teste.png"), "teste.csv")
    
    h._computeFog(0, 400)
    print len(h.gazeList)
