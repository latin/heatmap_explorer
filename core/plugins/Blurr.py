#!/usr/bin/env python
# -*- coding: utf-8 -*- 

from scipy import *
import math, cv2, numpy as np

class Blurr(object):
    def __init__(self, width = 640, height = 480, sigma = 5., blurrSize = 5):
        self.width   = width
        self.height  = height
        self.min     = 0.0
        self.cumWeights = np.ndarray((height, width), dtype=np.float)
        self.bleImg  = np.ndarray((height, width, 3), dtype=np.uint8)
        self.m            = max(width, height)*2
        self.x       = arange(0, self.m, 1, float)
        self.y       = self.x[:, newaxis]
        self.setSigma(sigma)
        # Creating blending layer with the specified color
        self.blendingLayer = np.ndarray((height, width, 3), dtype=np.uint8)
        self.setBlurrSize(blurrSize)
    # -------------------------------------------------

    def setBlurrSize(self, blurrSize):
        self.blurrSize = blurrSize

    def setSigma(self, sigma):
        self.sigma = sigma
        self.center = self.m/2
        self.gauss  = 4*exp(- ( ((self.x-self.center)**2) / (2*self.sigma**2) + ((self.y-self.center)**2) / (2*self.sigma**2 )) )

    def processImage(self, image, gazeList):
        imgSize = image.shape
        # Validate image size: should be the same as defined in the init method
        if imgSize[0] != self.height:
            raise ValueError('Image height is different from width internal value')
        if imgSize[1] != self.width:
            raise ValueError('Image width is different from width internal value')
        # Weight coefficients so so recent samples have higher coefficient
        length = len(gazeList)
        self.coeffs    = np.array( [math.pow(x, 2) for x in range(2, length+2, 1)] ).reshape((length, 1))
        #self.coeffs   /= np.sum(self.coeffs)
        self.coeffs   /= self.coeffs[length-1]
        # Compute sum of gaussian matrix
        self.computeWeights(gazeList)
        # Create the fog
        self.createBlendingLayer(image)
        self.blend(image)
        return self.bleImg
    
    # -------------------------------------------------

    def blend(self, img):
        for k in range(3):
            backgChannel = self.blendingLayer[:,:,k] * (1-self.cumWeights) * (1-self.cumWeightsMask)
            imageChannel = img[:,:,k] * self.cumWeights# * self.cumWeightsMask
            #blendChannel  = (1.0 - self.cumWeights)     * self.blendingLayer[:,:,k]
            self.bleImg[:,:,k] = backgChannel + imageChannel# + (1.0 - self.alphaBlend)*imageChannel + self.alphaBlend*blendChannel
        return self.bleImg
    
    # -------------------------------------------------

    def createBlendingLayer(self, img):
        self.blendingLayer = cv2.blur(img, (self.blurrSize, self.blurrSize))
    # -------------------------------------------------
    
    def computeWeights(self, gazeList):
        # Compute gaussian sum
        self.cumWeights.fill(self.min) 
        for i, item in enumerate(gazeList):
            if len(item) == 2:
                xx, yy = item[0], item[1]
            else: # assume [tstamp, x, y, ...]
                xx, yy = item[1], item[2]
            if xx >= 0 and xx < self.width and yy >= 0 and yy < self.height:
                bigX = self.center - xx
                bigY = self.center - yy
                wei  = self.gauss[ bigY:(bigY + self.height) , bigX:(bigX + self.width) ] * self.coeffs[i]
                # print self.cumWeights.shape, xx, yy, ":", wei.shape
                self.cumWeights += wei
        self.cumWeights = np.clip( self.cumWeights, 0.0, 1.0 )
        """
        minn = np.amin(self.cumWeights)
        maxx = np.amax(self.cumWeights)
        self.cumWeights = (self.cumWeights - minn) / (maxx - minn)
        """
        self.cumWeightsMask = (self.cumWeights>0.5)
        self.cumWeights *= self.cumWeightsMask


