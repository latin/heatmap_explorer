#!/usr/bin/env python
# -*- coding: utf-8 -*- 

from scipy import *
import math, cv2, numpy as np

class Heatmap(object):
    def __init__(self, width = 640, height = 480, sigma = 5., alphaImg = 1.0, alphaBlend = 0.5, color = (127, 127, 127)):
        self.width   = width
        self.height  = height
        self.alphaImg   = alphaImg
        self.alphaBlend = alphaBlend
        self.min     = 0.0
        self.cumWeights = np.ndarray((height, width), dtype=np.float)
        self.bleImg  = np.ndarray((height, width, 3), dtype=np.uint8)
        self.m            = max(width, height)*2
        self.x       = arange(0, self.m, 1, float)
        self.y       = self.x[:, newaxis]
        self.setSigma(sigma)
        # Creating blending layer with the specified color
        self.blendingLayer = np.ndarray((height, width, 3), dtype=np.uint8)
        self.setHeatColor(color)
        self.createImages = True
    # -------------------------------------------------

    def setHeatColor(self, color):
        self.color = color
        self.blendingLayer[:,:,0].fill(self.color[0])
        self.blendingLayer[:,:,1].fill(self.color[1])
        self.blendingLayer[:,:,2].fill(self.color[2])

    def setSigma(self, sigma):
        self.sigma = sigma
        self.center = self.m/2
        self.gauss  = 4*exp(- ( ((self.x-self.center)**2) / (2*self.sigma**2) + ((self.y-self.center)**2) / (2*self.sigma**2 )) )

    def processImage(self, image, gazeList):
        imgSize = image.shape
        # Validate image size: should be the same as defined in the init method
        if imgSize[0] != self.height:
            raise ValueError('Image height is different from width internal value')
        if imgSize[1] != self.width:
            raise ValueError('Image width is different from width internal value')
        # Weight coefficients so so recent samples have higher coefficient
        length = len(gazeList)
        self.coeffs    = np.array( [math.pow(x, 2) for x in range(2, length+2, 1)] ).reshape((length, 1))
        #self.coeffs   /= np.sum(self.coeffs)
        self.coeffs   /= self.coeffs[length-1]
        # Compute sum of gaussian matrix
        self.computeWeights(gazeList)
        # Create the heatmap
        self.createBlendingLayer(image)
        self.blend(image)
        if self.createImages:
            cv2.imwrite("gaussians.png", self.cumWeights*255)
            cv2.imwrite("original.png", image)
            cv2.imwrite("result.png", self.bleImg)                
        return self.bleImg
    
    # -------------------------------------------------

    def blend(self, img):
        img *= self.alphaImg    # Defines visibility of overall image
        tmpBlending = np.zeros(self.blendingLayer.shape)
        for k in range(3):
            imageChannel  = self.cumWeightsMask * img[:,:,k]
            blendChannel  = self.cumWeights     * self.blendingLayer[:,:,k]
            backgChannel = img[:,:,k] * (1-self.cumWeightsMask)
            self.bleImg[:,:,k] = backgChannel + (1.0 - self.alphaBlend)*imageChannel + self.alphaBlend*blendChannel
            tmpBlending[:,:,k] = self.alphaBlend*blendChannel
        if self.createImages:
            cv2.imwrite("blendingLayer.jpg", tmpBlending)
        return self.bleImg
    
    # -------------------------------------------------

    def createBlendingLayer(self, img):
        pass # Already created in the init method    
    # -------------------------------------------------

    def computeWeights(self, gazeList):
        # Compute gaussian sum
        self.cumWeights.fill(self.min) 
        for i, item in enumerate(gazeList):
            if len(item) == 2:
                xx, yy = item[0], item[1]
            else: # assume [tstamp, x, y, ...]
                xx, yy = item[1], item[2]
            if xx >= 0 and xx < self.width and yy >= 0 and yy < self.height:
                bigX = self.center - xx
                bigY = self.center - yy
                wei  = self.gauss[ bigY:(bigY + self.height) , bigX:(bigX + self.width) ] * self.coeffs[i]
                # print self.cumWeights.shape, xx, yy, ":", wei.shape
                self.cumWeights += wei
        self.cumWeights = np.clip( self.cumWeights, 0.0, 1.0 )
        #minn = np.amin(self.cumWeights)
        #maxx = np.amax(self.cumWeights)
        #self.cumWeights = (self.cumWeights - minn) / (maxx - minn)        
        self.cumWeightsMask = (self.cumWeights>0.2)
        self.cumWeights *= self.cumWeightsMask


