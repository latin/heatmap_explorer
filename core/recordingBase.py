class RecordingBase(object):
    
    def setPath(self, path):
        raise NotImplementedError
        
    def loadRecording(self):
        raise NotImplementedError
