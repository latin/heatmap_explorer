import QtQuick 2.0

Image {
    source: ""
    fillMode: Image.PreserveAspectFit
    property string providerName
    property int fps: 30
    property int totalFrames: videocapture.totalFrames
    property int currentFrame: 0
    property string workaround: ""
    cache: false

    signal paused();

    function newFrame(forward) {
        forward = typeof forward !== 'undefined' ? forward : true;
        if (currentFrame < totalFrames) {
            source = "";
            workaround += "a";
            source = "image://" + providerName + "/" + currentFrame + workaround;
            if (forward) {
                currentFrame++;
                workaround = "";
            }
        }
        else {
            paused();
        }
    }

    function play() {
        if (videocapture.ready) {
            timer.start();
            return true;
        }
        return false;
    }

    function pause() {
        timer.stop();
    }

    Timer {
        id: timer
        interval: 1000.0 / fps
        repeat: true
        onTriggered: {
            newFrame();
        }
    }
}
