# -*- coding: utf-8 -*-

from PyQt5.QtCore import pyqtProperty, QSize, Qt, QRect, QRectF, pyqtSignal
from PyQt5.QtWidgets import QWidget, QSizePolicy
from PyQt5.QtGui import QPainter, QColor, QLinearGradient, QPainterPath, QPen

from videosurface import VideoSurface, cv2pixmap

class DragState(object):
    def __init__(self, valueId, playing, initValue, initX):
        self.valueId = valueId
        self.playing = playing
        self.initValue = initValue
        self.initX = initX

class DoubleSeekbar(QWidget):
    videoSurfaceChanged = pyqtSignal(VideoSurface)
    minimumValueChanged = pyqtSignal(float)
    maximumValueChanged = pyqtSignal(float)
    value0Changed = pyqtSignal(float)
    value1Changed = pyqtSignal(float)
    playingChanged = pyqtSignal(bool)
    
    def __init__(self, parent=None):
        super(DoubleSeekbar, self).__init__(parent)
        self._videoSurface = None
        self._minVal = 0
        self._maxVal = 1
        self._val0 = 0
        self._val1 = 0
        self._playing = False
        self.frameRange = 0
        self.d = DoubleSeekbar.Dimensions(self)
        self.dragState = None
        
        self.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Minimum)
        self.playingChanged.connect(self.update)

    @pyqtProperty(int)
    def minimumValue(self):
        return self._minVal
    @minimumValue.setter
    def minimumValue(self, value):
        value = int(round(value))
        changed = value != self._minVal
        self._minVal = value
        if changed:
            self.minimumValueChanged.emit(self.minimumValue)
            self.update()
    
    @pyqtProperty(int)
    def maximumValue(self):
        return self._maxVal
    @maximumValue.setter
    def maximumValue(self, value):
        value = int(round(value))
        changed = value != self._maxVal
        self._maxVal = value
        if changed:
            self.maximumValueChanged.emit(self.maximumValue)
            self.update()

    def fixValue(self, value):
        return max(0, min(value, self.maximumValue))

    @pyqtProperty(bool)
    def enabled(self):
        return self.videoSurface is not None and self.videoSurface.recording is not None

    @pyqtProperty(int)
    def value0(self):
        return self._val0
    @value0.setter
    def value0(self, value):
        value = self.fixValue(int(round(value)))
        changed = value != self._val0
        self._val0 = value
        if changed:
            self.value0Changed.emit(self.value0)
            self.update()
    def updateValue0(self):
        self.value0 = max(self.value1 - self.frameRange, 0)
        
    @pyqtProperty(int)
    def value1(self):
        return self._val1
    @value1.setter
    def value1(self, value):
        value = self.fixValue(int(round(value)))
        changed = value != self._val1
        self._val1 = value
        if changed:
            self.value1Changed.emit(self.value1)
        self.updateValue0()
    def setValue1(self, value):
        self.value1 = value
        self.update()
        
    @pyqtProperty(bool)
    def playing(self):
        return self._playing
    @playing.setter
    def playing(self, value):
        changed = value != self._playing
        self._playing = value
        if self.videoSurface is not None:
            self.videoSurface.playing = self.playing
        if changed:
            self.playingChanged.emit(self.playing)
            self.update()
            
    @pyqtProperty(VideoSurface)
    def videoSurface(self):
        return self._videoSurface
    @videoSurface.setter
    def videoSurface(self, value):
        changed = value != self._videoSurface
        self._videoSurface = value
        self._videoSurface.recordingChanged.connect(self.updateFromRecording)
        if self._videoSurface.recording is not None:
            self.updateFromRecording()
        if changed:
            self.videoSurfaceChanged.emit(self._videoSurface)
            self.update()
            
    def updateFromRecording(self):
        self.minimumValue = 0
        self.maximumValue = self.videoSurface.recording.getFramesCount() - 1
        self.frameRange = 0
        self.videoSurface.recording.setWindowSize(self.frameRange)
        self.value1 = 0
        self.videoSurface.curFrameIdChanged.connect(self.setValue1)
    
    def sizeHint(self):
        return QSize(50, 75)
    
    def dragValueOffset(self, x):
        return (x - self.dragState.initX) / self.d.scale
        
    def dragValue0(self, x):
        valueOffset = max(0, min(self.dragState.initValue + self.dragValueOffset(x), self.value1))
        self.frameRange = int(self.value1 - valueOffset)
        self.videoSurface.recording.setWindowSize(self.frameRange)
        self.updateValue0()
        self.videoSurface.updateFrame()
        
    def dragValue1(self, x):
        self.value1 = self.dragState.initValue + self.dragValueOffset(x)
        self.videoSurface.curFrameId = self.value1
        self.videoSurface.updateFrame()
    
    def mousePressEvent(self, event):
        if not self.enabled:
            return
        if event.button() == Qt.LeftButton:
            dragging = False
            if self.d.marker0Contains(event.pos()):
                self.dragState = DragState(0, self.playing, self.value0, event.x())
                dragging = True
            if self.d.marker1Contains(event.pos()):
                self.dragState = DragState(1, self.playing, self.value1, event.x())
                dragging = True
            if not dragging and self.d.seekRect.contains(event.pos()):
                self.value1 = (event.x() - self.d.seekX) / self.d.scale + self.minimumValue
                self.videoSurface.curFrameId = self.value1
                self.videoSurface.updateFrame()
                self.dragState = DragState(1, self.playing, self.value1, event.x())
                dragging = True
            if dragging:
                self.playing = False
    
    def mouseMoveEvent(self, event):
        if not self.enabled:
            return
        if self.dragState is not None:
            if self.dragState.valueId == 0:
                self.dragValue0(event.x())
            elif self.dragState.valueId == 1:
                self.dragValue1(event.x())
    
    def mouseReleaseEvent(self, event):
        if not self.enabled:
            return
        if event.button() == Qt.LeftButton:
            if self.dragState is not None:
                if self.dragState.valueId == 0:
                    self.dragValue0(event.x())
                elif self.dragState.valueId == 1:
                    self.dragValue1(event.x())
                self.playing = self.dragState.playing
                self.dragState = None
            elif self.d.playPauseRect.contains(event.pos()):
                self.playing = not self.playing
    
    def paintEvent(self, e):
        qp = QPainter()
        qp.begin(self)
        qp.setRenderHint(QPainter.Antialiasing)
        self.drawWidget(qp)
        qp.end()

    def drawPlayPauseButton(self, qp):
        playPauseRect = QRectF(self.d.playPauseX, self.d.playPauseY, self.d.playPauseWidth, self.d.playPauseHeight)
        playPauseGradient = QLinearGradient(playPauseRect.topLeft(), playPauseRect.bottomLeft())
        playPauseGradient.setColorAt(0, QColor(107, 107, 107))
        playPauseGradient.setColorAt(1, QColor(34, 34, 34))
        playPausePath = QPainterPath()
        playPausePath.addRoundedRect(playPauseRect, self.d.playPauseRad, self.d.playPauseRad)
        pen = QPen(Qt.black, self.d.lineWidth)
        qp.setPen(pen)
        qp.fillPath(playPausePath, playPauseGradient)
        qp.drawPath(playPausePath)
        
        drawingColor = QColor(158, 158, 158)
        drawingPath = QPainterPath()
        drawingRect = self.d.playPauseDrawingRect
        qp.setPen(Qt.NoPen)
        if self.playing:
            pauseRect1 = QRect(drawingRect.x(), drawingRect.y(), drawingRect.width()/3, drawingRect.height())
            pauseRect2 = QRect(drawingRect.x() + 2*drawingRect.width()/3, drawingRect.y(), drawingRect.width()/3, drawingRect.height())
            
            qp.fillRect(pauseRect1, drawingColor)
            qp.fillRect(pauseRect2, drawingColor)
        else:
            startPointX = drawingRect.x()
            startPointY = drawingRect.y()
            endPointX1 = drawingRect.x() + drawingRect.width()
            endPointY1 = drawingRect.y() + drawingRect.height() / 2
            endPointY2 = drawingRect.y() + drawingRect.height()
            
            drawingPath.moveTo(startPointX, startPointY);
            drawingPath.lineTo(endPointX1, endPointY1);
            drawingPath.lineTo(startPointX, endPointY2);
            drawingPath.lineTo(startPointX, startPointY);
            
            qp.fillPath(drawingPath, drawingColor)

    def drawThumb(self, qp, rect, frameId, divide=False):
        if self.videoSurface is not None and self.videoSurface.recording is not None:
            qp.drawPixmap(rect, cv2pixmap(self.videoSurface.recording.getFrame(frameId)))
        else:
            qp.setBrush(Qt.black)
            qp.drawRect(rect)
        if divide:
            qp.setPen(QPen(Qt.black))
            x = rect.x() + rect.width() / 2
            qp.drawLine(x, rect.y(), x, rect.y() + rect.height())

    def drawWidget(self, qp):
        self.drawPlayPauseButton(qp)

        # Background
        backgroundRect = self.d.seekRect
        backgroundGradient = QLinearGradient(backgroundRect.topLeft(), backgroundRect.bottomLeft())
        backgroundGradient.setColorAt(0, QColor(57, 55, 56))
        backgroundGradient.setColorAt(1, QColor(146, 148, 150))
        backgroundPath = QPainterPath()
        backgroundPath.addRoundedRect(backgroundRect, self.d.seekRad, self.d.seekRad)
        pen = QPen(Qt.black, self.d.lineWidth)
        qp.setPen(pen)
        qp.fillPath(backgroundPath, backgroundGradient)
        qp.drawPath(backgroundPath)

        # Interval
        innerRect = QRectF(self.d.innerX, self.d.innerY, self.d.innerWidth, self.d.innerHeight)
        innerGradient = QLinearGradient(innerRect.topLeft(), innerRect.bottomLeft())
        innerGradient.setColorAt(0, QColor(100, 184, 255))
        innerGradient.setColorAt(1, QColor(80, 105, 236))
        qp.fillRect(innerRect, innerGradient)
        
        # Draw markers
        qp.setBrush(Qt.gray)
        qp.drawRect(self.d.marker0Rect)
        qp.drawRect(self.d.marker1Rect)
        
        # Draw thumbnails
        if self.value0 == self.value1:
            self.drawThumb(qp, self.d.singleThumbRect, self.value1, True)
        else:
            self.drawThumb(qp, self.d.thumb0Rect, self.value0)
            self.drawThumb(qp, self.d.thumb1Rect, self.value1)
    
    ############# DIMENSIONS ###############
    class Dimensions(object):
        def __init__(self, widget):
            self.widget = widget
            self.markerOffset = 10
            self.lineWidth = 1
            self.thumbAspect = 4. / 3
        
        # Widget
        @property
        def width(self):
            return self.widget.size().width()
        @property
        def height(self):
            return self.widget.size().height()
        # Seekbar
        @property
        def seekWidth(self):
            return self.width - 2*self.seekX
        @property
        def seekHeight(self):
            return min(self.height / 3, 10)
        @property
        def seekX(self):
            return self.thumbWidth - self.markerWidth
        @property
        def seekY(self):
            return self.height - self.seekHeight - self.markerOffset
        @property
        def seekRad(self):
            return self.seekHeight / 2
        @property
        def seekRect(self):
            return QRectF(self.seekX, self.seekY, self.seekWidth, self.seekHeight)
        # Inner seekbar content
        @property
        def scale(self):
            return self.seekWidth / (self.widget.maximumValue - self.widget.minimumValue)
        @property
        def innerX(self):
            return (self.widget.value0 - self.widget.minimumValue)*self.scale + self.seekX
        @property
        def innerY(self):
            return self.seekY + self.lineWidth
        @property
        def innerWidth(self):
            return self.marker1X - self.innerX
        @property
        def innerHeight(self):
            return self.seekHeight - 2*self.lineWidth
        # Thumbnails
        @property
        def thumb0X(self):
            return self.innerX - self.thumbWidth
        @property
        def thumb0Y(self):
            return 0
        @property
        def thumb1X(self):
            return self.marker1X
        @property
        def thumb1Y(self):
            return 0
        @property
        def thumbWidth(self):
            return self.thumbAspect * self.thumbHeight
        @property
        def thumbHeight(self):
            return self.height - self.seekHeight - 2*self.markerOffset
        @property
        def singleThumbRect(self):
            x = (self.thumb0X + self.thumb1X) / 2
            return QRect(x, self.thumb0Y, self.thumbWidth, self.thumbHeight)
        @property
        def thumb0Rect(self):
            return QRect(self.thumb0X, self.thumb0Y, self.thumbWidth, self.thumbHeight)
        @property
        def thumb1Rect(self):
            return QRect(self.thumb1X, self.thumb1Y, self.thumbWidth, self.thumbHeight)
        # Thumbnail markers
        @property
        def marker0X(self):
            return self.innerX - self.markerWidth
        @property
        def marker0Y(self):
            return self.thumb0Y + self.thumbHeight
        @property
        def marker1X(self):
            return (self.widget.value1 - self.widget.minimumValue)*self.scale + self.seekX
        @property
        def marker1Y(self):
            return self.thumb0Y + self.thumbHeight
        @property
        def markerWidth(self):
            return max(5, self.thumbWidth / 5)
        @property
        def markerHeight(self):
            return 2*self.markerOffset + self.seekHeight
        @property
        def marker0Rect(self):
            return QRect(self.marker0X, self.marker0Y, self.markerWidth, self.markerHeight)
        @property
        def marker1Rect(self):
            return QRect(self.marker1X, self.marker1Y, self.markerWidth, self.markerHeight)
        def marker0Contains(self, point):
            if self.marker0Rect.contains(point):
                return True
            if self.widget.value0 == self.widget.value1:
                sTRect = self.singleThumbRect
                return QRect(sTRect.x(), sTRect.y(), sTRect.width() / 2, sTRect.height()).contains(point)
            return self.thumb0Rect.contains(point)
        def marker1Contains(self, point):
            if self.marker1Rect.contains(point):
                return True
            if self.widget.value0 == self.widget.value1:
                sTRect = self.singleThumbRect
                return QRect(sTRect.x() + sTRect.width() / 2, sTRect.y(), sTRect.width() / 2, sTRect.height()).contains(point)
            return self.thumb1Rect.contains(point)
        # Play/Pause button
        @property
        def playPauseOffset(self):
            return self.markerOffset / 2
        @property
        def playPauseX(self):
            return 0
        @property
        def playPauseY(self):
            return self.thumbHeight + self.markerOffset - self.playPauseOffset
        @property
        def playPauseWidth(self):
            return self.seekX - self.playPauseOffset - self.markerWidth
        @property
        def playPauseHeight(self):
            return 2*self.playPauseOffset + self.seekHeight
        @property
        def playPauseRect(self):
            return QRect(self.playPauseX, self.playPauseY, self.playPauseWidth, self.playPauseHeight)
        @property
        def playPauseRad(self):
            return min(5, self.playPauseHeight / 8)
        @property
        def playPauseDrawingRect(self):
            off = 3
            l = min(self.playPauseWidth, self.playPauseHeight) - 2*off
            return QRect(self.playPauseX + (self.playPauseWidth - l)/2, self.playPauseY + (self.playPauseHeight - l)/2, l, l)
