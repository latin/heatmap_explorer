# -*- coding: utf-8 -*-

from PyQt5.QtCore import pyqtProperty, pyqtSignal, QTimer, QRect, Qt
from PyQt5.QtWidgets import QLabel
from PyQt5.QtGui import QImage, QPixmap, QPainter, QColor
import cv2

from core.pupilRecording  import *

def cv2pixmap(cvimg):
    cvimg = cv2.cvtColor(cvimg, cv2.cv.CV_BGR2RGB)
    qImage = QImage(cvimg, cvimg.shape[1], cvimg.shape[0], QImage.Format_RGB888)
    return QPixmap.fromImage(qImage)

class VideoSurface(QLabel):
    recordingChanged = pyqtSignal(PupilRecording)
    fpsChanged = pyqtSignal(int)
    playingChanged = pyqtSignal(bool)
    curFrameIdChanged = pyqtSignal(int)
    
    def __init__(self, settings, parent=None):
        super(VideoSurface, self).__init__(parent)

        self.settings = settings
        self.curFrame = None
        self._curFrameId = 0
        self.totalFrames = None
        self._recording = None
        self._fps = 30
        self._playing = False
        self.timer = QTimer(self)
        self.timer.setInterval(1000. / (self.fps * self.settings.speed))
        self.timer.setSingleShot(False)
        self.timer.timeout.connect(self.updateFrameId)
        self.fpsChanged.connect(self.resetTimer)
        self.playingChanged.connect(self.onPlayingChanged)
    
    @pyqtProperty(PupilRecording)
    def recording(self):
        return self._recording
    @recording.setter
    def recording(self, value):
        changed = value != self._recording
        self._recording = value
        if self._recording is not None:
            self.totalFrames = self._recording.getFramesCount()
            self.curFrameId = 0
            self.curFrame = None
        if changed:
            self.recordingChanged.emit(self._recording)
    @pyqtProperty(int)
    def fps(self):
        return self._fps
    @fps.setter
    def fps(self, value):
        changed = value != self._fps
        self._fps = value
        if changed:
            self.fpsChanged.emit(self.fps)
    @pyqtProperty(bool)
    def playing(self):
        return self._playing
    @playing.setter
    def playing(self, value):
        changed = value != self._playing
        self._playing = value
        if changed:
            self.playingChanged.emit(self.playing)
    @pyqtProperty(int)
    def curFrameId(self):
        return self._curFrameId
    @curFrameId.setter
    def curFrameId(self, value):
        changed = value != self._curFrameId
        self._curFrameId = value
        if changed:
            self.curFrameIdChanged.emit(self.curFrameId)

    def onPlayingChanged(self, playing):
        if playing:
            self.timer.start()
        else:
            self.timer.stop()

    def resetTimer(self):
        restart = self.timer.isActive()
        self.timer.stop()
        self.timer.setInterval(1000. / (self.fps * self.settings.speed))
        if restart:
            self.timer.start()

    def updateFrame(self):
        if self.recording is not None:
            self.recording.move2index(self.curFrameId)
            frame = self.recording.getCurrentFrame().copy()
            gazeList = self.recording.getGazeWindow()
            
            if self.settings.meanImage:
                frameList = self.recording.getFramesWindow()
                if len(frameList) > 0:
                    frame = np.mean(frameList, axis=0).astype(frame.dtype)
            if self.settings.imageBW:
                gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                frame = np.dstack((gray, gray, gray))
            
            if len(gazeList) > 2:
                processor = self.settings.processor
                frame = processor.processImage(frame, gazeList)
            self.curFrame = cv2pixmap(frame)
            self.update()

    def updateFrameId(self):
        self.updateFrame()
        if self.totalFrames is None or self.curFrameId < self.totalFrames - 1:
            self.curFrameId += 1
    
    def drawFrame(self, qp):
        scaled = self.curFrame.scaled(self.size(), Qt.KeepAspectRatio)
        qp.drawPixmap((self.width() - scaled.width()) / 2, (self.height() - scaled.height()) / 2, scaled)
    
    def paintEvent(self, e):
        qp = QPainter()
        qp.begin(self)
        qp.setRenderHint(QPainter.Antialiasing)
        qp.fillRect(QRect(0, 0, self.width(), self.height()), QColor(0, 0, 0))
        if self.curFrame is not None:
            self.drawFrame(qp)
        qp.end()
