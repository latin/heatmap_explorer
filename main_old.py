
from PyQt5.QtCore import QObject, QUrl, QMetaObject
from PyQt5.QtWidgets import QApplication
from PyQt5.QtQml import QQmlApplicationEngine
from image_provider import VideoProvider
import os
import cv2
import numpy as np

from core.plugins.Heatmap import *
from core.plugins.Fog     import *
from core.plugins.Blurr   import *

class DummyProcessor(object):
    def processImage(frame, gaze_list):
        return frame

class MainWindow(QObject):
    def __init__(self):
        QObject.__init__(self)
        self.app = QApplication(["Heat Map Explorer"])
        self.engine = QQmlApplicationEngine()
        self.context = self.engine.rootContext()
        
        self.image_provider = VideoProvider("video", self.engine, self.process_frame)
        #self.thumb_provider = VideoProvider("thumb", self.engine, self.process_frame, self.image_provider.cap)
        
        self.engine.load(QUrl("main.qml"))
        self.root = self.engine.rootObjects()[0]
        
        # Setup video backend
        self.camera_video = self.root.findChild(QObject, "videoSurface")
        self.image_provider.setup(self.camera_video)
        self.seekbar = self.root.findChild(QObject, "seekbar")
        #self.thumb_provider.setup(self.seekbar)
        self.image_provider.setup(self.seekbar)
        
        # Find qml elements
        self.play_pause_button = self.root.findChild(QObject, "playPauseButton")
        self.mean_image_checkbox = self.root.findChild(QObject, "meanImageCheckbox")
        self.bw_checkbox = self.root.findChild(QObject, "bwCheckbox")
        self.video_mode_combo = self.root.findChild(QObject, "videoModeCombo")
        self.transparency_slider = self.root.findChild(QObject, "transparencySlider")
        self.intensity_slider = self.root.findChild(QObject, "intensitySlider")
        self.size_slider = self.root.findChild(QObject, "sizeSlider")
        self.color_picker = self.root.findChild(QObject, "heatColor")
        
        # Populate video mode combobox
        self.video_modes = ["Fog", "Heatmap", "Blur"]
        self.video_mode_combo.setProperty("model", self.video_modes)
        
        # Setup open action
        self.file_dialog = self.root.findChild(QObject, "fileDialog")
        self.file_dialog.accepted.connect(self.on_open)
        
        self.root.show()
        self.app.exec_()
    
    def process_frame(self, frame, gaze_list, frame_list):
        if self.mean_image_checkbox.property("checked"):
            frame = np.mean(frame_list, axis=0).astype(frame.dtype)
        if self.bw_checkbox.property("checked"):
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            frame = np.dstack((gray, gray, gray))
        if len(gaze_list) > 2:
            selected_mode = self.video_mode_combo.property("currentText")
            if selected_mode == self.video_modes[0]:
                sigma = self.size_slider.property("value")
                alpha_img = self.transparency_slider.property("value")
                intensity = self.intensity_slider.property("value")
                processor = Fog(640, 480, sigma = sigma, fogIntensity = intensity, alphaImg = alpha_img)
            elif selected_mode == self.video_modes[1]:
                sigma = self.size_slider.property("value")
                alpha_img = self.transparency_slider.property("value")
                intensity = self.intensity_slider.property("value")
                color = self.color_picker.property("color")
                processor = Heatmap(640, 480, sigma = sigma, alphaImg = alpha_img, alphaBlend = intensity, color = (color.blue(), color.green(), color.red()))
            elif selected_mode == self.video_modes[2]:
                sigma = self.size_slider.property("value")
                intensity = self.intensity_slider.property("value")
                processor = Blurr(640, 480, sigma = sigma, blurrSize = int((intensity*0.9+0.1)*100))
            else:
                processor = DummyProcessor()
            frame = processor.processImage(frame, gaze_list)
        return frame
    
    def on_open(self):
        path = self.file_dialog.property("fileUrl").toLocalFile()
        self.image_provider.cap.open(path)
        self.play_pause_button.clicked.emit()
        QMetaObject.invokeMethod(self.camera_video, "play")

MainWindow()