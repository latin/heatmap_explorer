import QtQuick 2.0
import QtQuick.Controls 1.2

ApplicationWindow {
    title: qsTr("Test")

    width: 1024
    height: 768

    signal applicationClosing();

    CVVideo {
        objectName: "cameraVideo"
        anchors.fill: parent
    }

    onClosing: {
        applicationClosing();
    }
}
