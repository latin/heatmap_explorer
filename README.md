Heatmap Explorer is a interaction gaze data visualization tool developed by [LaTIn - IME - USP](https://latin.ime.usp.br/). 

More information can be found in the [IHC'16 paper](https://latin.ime.usp.br/publications/heatmap-explorer/).

# Requirements

1. PyQt5
2. Pillow
3. numpy
4. OpenCV 2.X

# Running

Simply execute in the terminal:

    $ python main.py

Open the data folder (an example can be found [here](https://latin.ime.usp.br/media/papers/data/paper_prototype.zip)) with `File->Open` or `Ctrl+O` (`Command+O` on OS X). Heatmap Explorer will take a few seconds to load and preprocess the gaze data. The interface looks as follows:

![interface](imgs/interface.png)

In `Video mode` you can select different visualization techniques, the color of the heatmap (when applicable) and set the transparency of the original video, and the intensity and size of the heatmap. 

In the `Video controls` you can choose a different playback rate and choose whether you want to use the original video, the mean image video and/or the black and white video (more details in the paper). You can also set the interval for the heatmap. 

There are two video thumbnails on the seekbar. The rightmost thumbnail represents the current frame and the leftmost one shows the first frame on the interval. You can adjust this interval by dragging the thumbnails.
