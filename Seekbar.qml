import QtQuick 2.0

Rectangle {
    id: bar
    radius: 2
    property int maximumValue: 0
    property int value: 0
    property int leftValue: Math.max(0, value - bufferSize)
    property int bufferSize: 30
    color: "lightgray"
    border.color: "gray"
    border.width: 1
    property bool dragging: valueControlArea.pressed
    property int controlRectWidth: 10
    property int controlRectHeight: height * 1.5
    property int thumbWidth: 40
    property int thumbHeight: 30
    property int thumbMargin: 5
    property string providerName: ""

    signal clicked(var mouse)
    signal valueDragged(int value)
    signal valueRequested(int value)

    onClicked: {
        if (bufferControlArea.drag.active) return;
        var value = mouse.x * maximumValue / bar.width;
        valueRequested(value);
    }

    onValueDragged: {
        if (bufferControlArea.drag.active) return;
        valueRequested(value);
    }

    MouseArea {
        anchors.fill: parent
        onClicked: bar.clicked(mouse)
    }

    Rectangle {
        id: bufferRect
        color: "lightsteelblue"
        border.color: "steelblue"
        x: (leftValue / maximumValue) * bar.width
        width: ((value - leftValue) / maximumValue) * parent.width
        height: parent.height
    }

    Rectangle {
        id: rightThumb
        x: (value / maximumValue) * bar.width - width / 2
        anchors.bottom: parent.top
        anchors.bottomMargin: thumbMargin
        width: thumbWidth
        height: thumbHeight

        color: "gray"

        Image {
            anchors.fill: parent
            source: providerName ? "image://" + providerName + "/" + "thumb" + value : "";
        }
    }

    Rectangle {
        id: leftThumb
        x: (leftValue / maximumValue) * bar.width - width / 2
        anchors.top: parent.bottom
        anchors.topMargin: thumbMargin
        width: thumbWidth
        height: thumbHeight

        color: "gray"

        Image {
            anchors.fill: parent
            source: providerName ? "image://" + providerName + "/" + "thumb" + leftValue : "";
        }
    }

    Rectangle {
        id: valueControlRect
        width: controlRectWidth
        height: controlRectHeight
        anchors.verticalCenter: parent.verticalCenter
        x: (value / maximumValue) * bar.width - width / 2
        border.color: "gray"
        color: "lightgray"

        MouseArea {
            id: valueControlArea
            anchors.fill: parent
            drag {
                target: parent
                minimumX: -valueControlRect.width / 2
                maximumX: bar.width - valueControlRect.width / 2
                axis: Drag.XAxis
            }
            onMouseXChanged: {
                if (drag.active) {
                    var newValue = maximumValue * (valueControlRect.x + valueControlRect.width / 2) / bar.width;
                    valueDragged(newValue);
                }
            }
        }
    }

    Rectangle {
        id: bufferControlRect
        width: controlRectWidth
        height: controlRectHeight
        anchors.verticalCenter: parent.verticalCenter
        x: (leftValue / maximumValue) * bar.width - width / 2
        border.color: "gray"
        color: "lightgray"

        MouseArea {
            id: bufferControlArea
            anchors.fill: parent
            drag {
                target: parent
                minimumX: -bufferControlRect.width / 2
                maximumX: valueControlRect.x
                axis: Drag.XAxis
            }
            onMouseXChanged: {
                if (drag.active) {
                    var newValue = maximumValue * (bufferControlRect.x + bufferControlRect.width / 2) / bar.width;
                    bar.bufferSize = bar.value - newValue;
                }
            }
        }
    }
}
