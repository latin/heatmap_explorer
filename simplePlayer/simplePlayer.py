import sys, os

__cam = os.path.split( os.path.abspath(__file__) )[0] + "/../core"
sys.path.append(__cam)
from pupilRecording import *

__cam = os.path.split( os.path.abspath(__file__) )[0] + "/../core/plugins"
sys.path.append(__cam)
from Heatmap import *
from Fog     import *
from Blurr   import *

# ==================================================
def windowPlayer(pr, hm, wsize):
    pr.setWindowSize(wsize)
    cv2.namedWindow("Player", cv2.WINDOW_NORMAL)
    backupIndex = pr.getCurrentIndex()
    pr.move2index(0)
    while pr.getCurrentIndex() < pr.getFramesCount():
        frame    = pr.getCurrentFrame()
        gazeList = pr.getGazeWindow()
        if len(gazeList) > 2:
            hmImg    = hm.processImage(frame, gazeList)
            """
            for gaze in gazeList:
                cv2.circle(frame, tuple(gaze), 5, (255, 0, 0), -1)
            """
            cv2.imshow("Player", hmImg)
        key = cv2.waitKey(17)
        if key == 27:
            break
        pr.nextFrame()
    pr.move2index(backupIndex)


if __name__ == '__main__':
    pr = PupilRecording(recordPath = "../../pupil_recordings/gui/", maxWindowSize = 30)
    if 1:
        hm = Heatmap(640, 480, sigma = 15.0, alphaImg = 0.5, alphaBlend = 0.8, color = (0, 0, 255))
    elif 0:
        hm = Fog(640, 480, sigma = 15.0, fogIntensity = 0.2, alphaImg = 0.35)
    else:
        hm = Blurr(640, 480, sigma = 17.0, blurrSize = 20)
    windowPlayer(pr, hm, wsize = 30)


