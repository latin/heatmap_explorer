# -*- coding: utf-8 -*-

import sys
from PyQt5.QtCore import QThread, pyqtSignal, Qt
from PyQt5.QtWidgets import QMainWindow, QAction, qApp, QApplication, QFileDialog, QLabel, QGridLayout, QWidget, QGroupBox, QComboBox, QPushButton, QColorDialog, QSlider, QSizePolicy, QVBoxLayout, QHBoxLayout, QCheckBox
from PyQt5.QtGui import QColor

from core.pupilRecording import *
from core.plugins.Heatmap import *
from core.plugins.Fog     import *
from core.plugins.Blurr   import *

from Gui import DoubleSeekbar, VideoSurface

class DataLoadingThread(QThread):
    dataLoaded = pyqtSignal()
    
    def __init__(self):
        super(DataLoadingThread, self).__init__()
        self.pr = None
        self.path = ''

    def run(self):
        self.pr = PupilRecording(recordPath = self.path)
        self.dataLoaded.emit()

class DummyProcessor(object):
    def processImage(self, frame, gaze_list):
        return frame

class Settings(object):
    def __init__(self, window):
        self.window = window
        self.processors = ["Fog", "Heatmap", "Blur", "Original (no gaze)"]
        self.speeds = [("1.0x", 1), ("0.5x", 0.5), ("0.2x", 0.2), ("0.1x", 0.1)]
        self._w = 640
        self._h = 480

    def connectSignals(self):
        self.window.processorCombo.currentTextChanged.connect(self._resetProcessor)
        self.window.transparencySlider.valueChanged.connect(self._resetProcessor)
        self.window.intensitySlider.valueChanged.connect(self._resetProcessor)
        self.window.sizeSlider.valueChanged.connect(self._resetProcessor)
        self._resetProcessor()

    @property
    def width(self):
        return self._w

    @width.setter
    def width(self, w):
        self._w = w
        self._resetProcessor()

    @property
    def height(self):
        return self._h

    @height.setter
    def height(self, h):
        self._h = h
        self._resetProcessor()

    def _resetProcessor(self):
        processorName = self.window.processorCombo.currentText()
        alphaImg = self.window.transparencySlider.value() / 100.0
        intensity = self.window.intensitySlider.value() / 100.0
        sigma = self.window.sizeSlider.value()
        w, h = self.width, self.height
        if processorName == self.processors[0]:
            self.processor = Fog(w, h, sigma = sigma, fogIntensity = intensity, alphaImg = alphaImg)
        elif processorName == self.processors[1]:
            color = self.window.color
            self.processor = Heatmap(w, h, sigma = sigma, alphaImg = alphaImg, alphaBlend = intensity, color = (color.blue(), color.green(), color.red()))
        elif processorName == self.processors[2]:
            self.processor = Blurr(w, h, sigma = sigma, blurrSize = int((intensity*0.9+0.1)*100))
        else:
            self.processor = DummyProcessor()
        
    @property
    def imageBW(self):
        return self.window.imageBWCheckbox.isChecked()
        
    @property
    def meanImage(self):
        return self.window.meanImageCheckbox.isChecked()
    
    @property
    def speed(self):
        return self.window.speedCombo.currentData()

class Window(QMainWindow):
    def __init__(self):
        super(Window, self).__init__()
        self.pr = None
        self.bufferSize = 30
        self.loadingThread = DataLoadingThread()
        self.loadingThread.dataLoaded.connect(self.onRecordingLoaded)
        self.color = QColor(255, 0, 0)
        self.settings = Settings(self)
        
        self.initUI()
        self.settings.connectSignals()
        
    def onRecordingLoaded(self):
        if self.loadingThread.pr.ready:
            self.pr = self.loadingThread.pr
            self.pr.setWindowSize(self.bufferSize)
            self.videoSurface.recording = self.pr
            self.statusBar().showMessage('Ready')
            self.seekbar.playing = True
            self.settings.width = self.pr.targetWidth
            self.settings.height = self.pr.targetHeight
        else:
            self.statusBar().showMessage('No data loaded')
        
    def open(self):
        path = QFileDialog.getExistingDirectory(self, 'Open Recording Directory', '', QFileDialog.ShowDirsOnly | QFileDialog.DontResolveSymlinks)
        self.statusBar().showMessage('Loading recording...')
        self.loadingThread.path = path
        self.loadingThread.start()
    
    def showDialog(self):
        col = QColorDialog.getColor(self.color)

        if col.isValid():
            self.color = col
            self.colorButton.setStyleSheet("QWidget { background-color: %s }"%self.color.name())
            self.videoSurface.updateFrame()
    
    def createMenu(self):
        openAction = QAction('&Open', self)        
        openAction.setShortcut('Ctrl+O')
        openAction.setStatusTip('Open recording')
        openAction.triggered.connect(self.open)
        
        exitAction = QAction('&Exit', self)        
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setStatusTip('Exit application')
        exitAction.triggered.connect(qApp.quit)
        
        self.statusBar()
        
        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&File')
        fileMenu.addAction(exitAction)
        fileMenu.addAction(openAction)
    
    def createVideoModeGroup(self):
        videoModeGroup = QGroupBox('Video mode')
        modeGrid = QGridLayout(videoModeGroup)
        self.processorCombo = QComboBox()
        for processor in self.settings.processors:
            self.processorCombo.addItem(processor)
        self.processorCombo.currentTextChanged.connect(self.videoSurface.updateFrame)
        
        self.colorButton = QPushButton()
        self.colorButton.setStyleSheet("QWidget { background-color: %s }"%self.color.name())
        self.colorButton.clicked.connect(self.showDialog)
        colorLabel = QLabel("Color")

        maxSliderHeight = 200
        self.transparencySlider = QSlider(Qt.Vertical)
        self.transparencySlider.setMaximumHeight(maxSliderHeight)
        self.transparencySlider.setMinimum(10)
        self.transparencySlider.setMaximum(100)
        self.transparencySlider.setValue(80)
        self.transparencySlider.valueChanged.connect(self.videoSurface.updateFrame)
        
        self.intensitySlider = QSlider(Qt.Vertical)
        self.intensitySlider.setMaximumHeight(maxSliderHeight)
        self.intensitySlider.setMinimum(0)
        self.intensitySlider.setMaximum(100)
        self.intensitySlider.setValue(40)
        self.intensitySlider.valueChanged.connect(self.videoSurface.updateFrame)
        
        self.sizeSlider = QSlider(Qt.Vertical)
        self.sizeSlider.setMaximumHeight(maxSliderHeight)
        self.sizeSlider.setMinimum(1)
        self.sizeSlider.setMaximum(20)
        self.sizeSlider.setValue(5)
        self.sizeSlider.valueChanged.connect(self.videoSurface.updateFrame)
        
        transparencyLabel = QLabel("Transp.")
        intensityLabel = QLabel("Intens.")
        sizeLabel = QLabel("Size")
        
        modeGrid.addWidget(self.processorCombo, 0, 0, 1, 3)
        modeGrid.addWidget(self.colorButton, 1, 1, Qt.AlignHCenter)
        modeGrid.addWidget(colorLabel, 2, 1, Qt.AlignHCenter)
        modeGrid.addWidget(self.transparencySlider, 3, 0, Qt.AlignHCenter)
        modeGrid.addWidget(self.intensitySlider, 3, 1, Qt.AlignHCenter)
        modeGrid.addWidget(self.sizeSlider, 3, 2, Qt.AlignHCenter)
        modeGrid.addWidget(transparencyLabel, 4, 0, Qt.AlignHCenter)
        modeGrid.addWidget(intensityLabel, 4, 1, Qt.AlignHCenter)
        modeGrid.addWidget(sizeLabel, 4, 2, Qt.AlignHCenter)
        modeGrid.setRowStretch(3, 1)
        modeGrid.setRowStretch(5, 1)

        return videoModeGroup
    
    def createVideoControlsGroup(self):
        videoControlsGroup = QGroupBox('Video controls')
        
        self.meanImageCheckbox = QCheckBox('Mean image')
        self.meanImageCheckbox.stateChanged.connect(self.videoSurface.updateFrame)
        self.imageBWCheckbox = QCheckBox('Image B&W')
        self.imageBWCheckbox.stateChanged.connect(self.videoSurface.updateFrame)
        self.seekbar = DoubleSeekbar()
        self.seekbar.videoSurface = self.videoSurface
        self.speedCombo.setMaximumWidth(150)
        self.speedCombo.currentTextChanged.connect(self.videoSurface.resetTimer)
        
        checkVBox = QVBoxLayout()
        checkVBox.addWidget(self.meanImageCheckbox)
        checkVBox.addWidget(self.imageBWCheckbox)
        
        playerVBox = QVBoxLayout()
        playerVBox.addWidget(self.seekbar)
        playerVBox.addWidget(self.speedCombo)
        
        hbox = QHBoxLayout()
        hbox.addLayout(playerVBox)
        hbox.addLayout(checkVBox)
        
        videoControlsGroup.setLayout(hbox)
        
        return videoControlsGroup
    
    def initUI(self):
        self.speedCombo = QComboBox()
        for speed in self.settings.speeds:
            self.speedCombo.addItem(speed[0], speed[1])
        self.videoSurface = VideoSurface(self.settings)
        
        layoutWidget = QWidget()
        grid = QGridLayout(layoutWidget)
        grid.setSpacing(10)
        grid.addWidget(self.videoSurface, 0, 0)
        grid.addWidget(self.createVideoModeGroup(), 0, 1)
        grid.addWidget(self.createVideoControlsGroup(), 1, 0, 1, 2)
        grid.setColumnStretch(0, 1)
        grid.setRowStretch(0, 1)
        self.setCentralWidget(layoutWidget)
        
        self.createMenu()
        
        self.setGeometry(300, 300, 1024, 768)
        self.setWindowTitle('Heat Map Explorer')
        
        self.show()


if __name__ == '__main__':
    
    app = QApplication(['Heat Map Explorer'])
    ex = Window()
    sys.exit(app.exec_())