from PyQt5.QtCore import QThread, QObject, QUrl, pyqtProperty, pyqtSignal
from PyQt5.QtGui import QPixmap, QColor
from PyQt5.QtQuick import QQuickImageProvider, QQuickView
from PIL.ImageQt import ImageQt
from PIL import Image
import numpy as np
import cv2
import re

from core.pupilRecording import *

'''
TODO Update this
Usage:
# Create QML Application Engine
engine = QQmlApplicationEngine()
cvvideo = # Find CVVideo object from QML

# Create Video Provider
provider = VideoProvider("provider_name", engine)
provider.cap.open(video_capture_id)
# Connect provider to CVVideo object
provider.setup(cvvideo)

--- QML ---
// Create applicationClosing signal (workaround) in your ApplicationWindow
signal applicationClosing();

onClosing: {
    applicationClosing();
}
'''

def array2pixmap(array):
    qt_image = ImageQt(Image.fromarray(cv2.cvtColor(array, cv2.COLOR_BGR2RGB)))
    return QPixmap.fromImage(qt_image)

class CameraCapture(QObject):
    def __init__(self):
        QObject.__init__(self)
        self.pr = None
        self.buffer_size = 30
    
    def open(self, data_path):
        self.pr = PupilRecording(recordPath = data_path)
        self.pr.setWindowSize(self.buffer_size)
        self.readyChanged.emit()
        self.totalFramesChanged.emit()
        
    totalFramesChanged = pyqtSignal()
    readyChanged = pyqtSignal()
    
    @pyqtProperty(int)
    def bufferSize(self):
        return self.buffer_size
    
    @bufferSize.setter
    def bufferSize(self, new_size):
        self.buffer_size = new_size
        if self.pr is not None:
            self.pr.setWindowSize(self.buffer_size)
    
    @pyqtProperty(int, notify=readyChanged)
    def ready(self):
        return self.pr is not None
    
    @pyqtProperty(int, notify=totalFramesChanged)
    def totalFrames(self):
        if self.pr is None:
            return -1
        return self.pr.getFramesCount()
    
    def getFrame(self, id, frame_only=False):
        if frame_only:
            return self.pr.getFrame(id).copy()
        else:
            self.pr.move2index(id)
            return self.pr.getCurrentFrame().copy(), self.pr.getGazeWindow(), self.pr.getFramesWindow()
    
class VideoProvider(QQuickImageProvider):
    def __init__(self, name, engine, filter_func=lambda frame, gaze_list, frame_list: frame, cap = None):
        QQuickImageProvider.__init__(self, QQuickImageProvider.Pixmap)
        self.name = name
        self.filter_func = filter_func
        
        self.engine = engine
        self.engine.addImageProvider(self.name, self)

        if cap is None:
            self.cap = CameraCapture()
            self.engine.rootContext().setContextProperty('videocapture', self.cap)
        else:
            self.cap = cap

    def requestPixmap(self, id, size):
        is_thumb = False
        if id.startswith("thumb"):
            id = id[5:]
            is_thumb = True
        # Remove aaaaa workaround
        id = re.sub(r'a*$', '', id)
        try:
            id = int(id)
        except ValueError:
            id = -1
        if id >= 0 and id < self.cap.totalFrames:
            # TODO For some reason this is not working... :(
            if is_thumb:
                frame = self.cap.getFrame(id, True)
                frame = cv2.resize(frame, (100, 100*frame.shape[0]/frame.shape[1]))
            else:
                frame, gaze_list, frame_list = self.cap.getFrame(id)
                frame = self.filter_func(frame, gaze_list, frame_list)
            pixmap = array2pixmap(frame)
            return pixmap, pixmap.size()
        default_pixmap = array2pixmap(np.random.randint(0, 255, (100, 100, 3)).astype(np.uint8))
        return default_pixmap, default_pixmap.size()
    
    def setup(self, cvvideo):
        cvvideo.setProperty('providerName', self.name)

if __name__=='__main__':
    from PyQt5.QtWidgets import QApplication
    from PyQt5.QtQml import QQmlApplicationEngine

    class ImageProviderGUI(QObject):
        def __init__(self):
            QObject.__init__(self)
            self.app = QApplication(["Video Provider Test"])
            self.engine = QQmlApplicationEngine()
            self.context = self.engine.rootContext()
            
            self.image_provider = VideoProvider("cam", self.engine)
            
            self.engine.load(QUrl("test.qml"))
            self.root = self.engine.rootObjects()[0]
            
            self.camera_video = self.root.findChild(QObject, "cameraVideo")
            self.image_provider.setup(self.camera_video)
            self.image_provider.cap.open("../pupil_recordings/001")
            self.image_provider.cap.start()
            
            self.root.show()
            self.app.exec_()

    ImageProviderGUI()