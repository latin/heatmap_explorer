import QtQuick 2.0
import QtQuick.Window 2.0
import QtQuick.Controls 1.2
import QtQuick.Dialogs 1.0
import QtQuick.Layouts 1.1

ApplicationWindow {
    id: win
    title: qsTr("Heat Map Explorer")
    width: 1024
    height: 768

    menuBar: MenuBar {
        Menu {
            title: "File"
            MenuItem {
                text: "&Open"
                shortcut: "Ctrl+O"
                onTriggered: {
                    // TODO: pause video
                    fileDialog.visible = true;
                }
            }
        }
    }

    SplitView {
        anchors.fill: parent
        orientation: Qt.Vertical

        SplitView {
            orientation: Qt.Horizontal
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.minimumHeight: 480
            Layout.margins: 10

            Rectangle {
                id: videoRegion
                border.color: "dark gray"
                color: "black"
                Layout.fillHeight: true
                Layout.fillWidth: true
                Layout.minimumWidth: 400

                CVVideo {
                    id: video
                    anchors.fill: parent
                    objectName: "videoSurface"
                    fps: 30 * speedModel.get(speedCombo.currentIndex).value

                    onPaused: {
                        if (playPauseButton) playPauseButton.playing = false;
                    }
                }
            }

            ColumnLayout {
                id: heatmapControls
                Layout.minimumWidth: 100
                Layout.fillHeight: true
                Layout.margins: 5

                Text {
                    text: "Video mode:"
                }
                ComboBox {
                    id: videoModeCombo
                    objectName: "videoModeCombo"
                    width: meanImageCheckbox.width

                    onCurrentIndexChanged: {
                        video.newFrame(false);
                    }
                }

                Rectangle {
                    id: selectedColor
                    objectName: "heatColor"
                    Layout.alignment: Qt.AlignHCenter
                    width: 50
                    height: width
                    radius: width
                    color: "red"

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            colorDialog.color = selectedColor.color;
                            colorDialog.visible = true;
                        }
                    }
                }

                Text {
                    id: colorLabel
                    Layout.alignment: Qt.AlignHCenter
                    text: "Color"
                }

                GridLayout {
                    width: parent.width
                    Layout.maximumHeight: 200
                    Layout.preferredHeight: videoRegion.height - 3 * selectedColor.height
                    columns: 3

                    Slider {
                        id: transparencySlider
                        objectName: "transparencySlider"
                        Layout.alignment: Qt.AlignHCenter
                        Layout.fillHeight: true
                        orientation: Qt.Vertical

                        minimumValue: 0.1
                        maximumValue: 1
                        value: 0.8

                        onValueChanged: {
                            video.newFrame(false);
                        }
                    }
                    Slider {
                        id: intensitySlider
                        objectName: "intensitySlider"
                        Layout.alignment: Qt.AlignHCenter
                        Layout.fillHeight: true
                        orientation: Qt.Vertical

                        minimumValue: 0
                        maximumValue: 1
                        value: 0.4

                        onValueChanged: {
                            video.newFrame(false);
                        }
                    }
                    Slider {
                        id: sizeSlider
                        objectName: "sizeSlider"
                        Layout.alignment: Qt.AlignHCenter
                        Layout.fillHeight: true
                        orientation: Qt.Vertical

                        minimumValue: 1
                        maximumValue: 20
                        value: 5

                        onValueChanged: {
                            video.newFrame(false);
                        }
                    }
                    Text {
                        text: "Transp."
                    }
                    Text {
                        text: "Intens."
                    }
                    Text {
                        text: "Size"
                    }
                }
            }
        }

        GroupBox {
            title: "Video controls"
            Layout.fillWidth: true
            Layout.minimumHeight: 100
            Layout.maximumHeight: 200
            Layout.leftMargin: 10
            Layout.rightMargin: 10
            Layout.bottomMargin: 10

            Rectangle {
                id: playPauseButton
                objectName: "playPauseButton"
                anchors.top: parent.top
                anchors.left: parent.left
                property bool playing: false
                property bool pressed: playButtonMouseArea.pressed
                width: 30
                height: width
                radius: width
                color: "transparent"

                signal clicked()

                onClicked: playButtonMouseArea.clicked(0)

                Image {
                    anchors.fill: parent
                    source: playPauseButton.playing ? "imgs/button_black_pause.png" : "imgs/button_black_play.png"
                    opacity: parent.pressed ? 0.8 : 1
                }

                MouseArea {
                    id: playButtonMouseArea
                    anchors.fill: parent

                    onClicked: {
                        playPauseButton.playing = !playPauseButton.playing;
                        if (playPauseButton.playing) {
                            if (!video.play()) playPauseButton.playing = false;
                        }
                        else video.pause();
                    }
                }
            }

            Seekbar {
                id: seekbar
                objectName: "seekbar"
                z: 100
                anchors.verticalCenter: playPauseButton.verticalCenter
                anchors.left: playPauseButton.right
                anchors.leftMargin: 5
                width: win.width - playPauseButton.width - checkboxes.width - 10 * anchors.leftMargin - parent.anchors.leftMargin - parent.anchors.rightMargin
                Layout.fillWidth: true
                height: 20

                maximumValue: video.totalFrames > 0 ? video.totalFrames : video.currentFrame
                value: video.currentFrame

                onValueRequested: {
                    video.currentFrame = value;
                    video.newFrame(false);
                }

                onBufferSizeChanged: {
                    videocapture.bufferSize = bufferSize;
                    video.newFrame(false);
                }
            }

            ColumnLayout {
                id: checkboxes

                anchors.top: parent.top
                anchors.left: seekbar.right
                anchors.leftMargin: 5
                CheckBox {
                    id: meanImageCheckbox
                    objectName: "meanImageCheckbox"
                    text: "Mean image"

                    onCheckedChanged: {
                        video.newFrame(false);
                    }
                }
                CheckBox {
                    id: bwCheckbox
                    objectName: "bwCheckbox"
                    text: "Video B&W"

                    onCheckedChanged: {
                        video.newFrame(false);
                    }
                }
            }

            ComboBox {
                id: speedCombo
                anchors.top: playPauseButton.bottom
                anchors.topMargin: 10
                anchors.left: parent.left
                model: ListModel {
                    id: speedModel
                    ListElement { text: "1.0x"; value: 1 }
                    ListElement { text: "0.5x"; value: 0.5 }
                    ListElement { text: "0.2x"; value: 0.2 }
                    ListElement { text: "0.1x"; value: 0.2 }
                }
            }
        }
    }

    FileDialog {
        id: fileDialog
        objectName: "fileDialog"

        title: "Please choose a data folder"
        selectFolder: true
    }

    ColorDialog {
        id: colorDialog
        title: "Select the map color"
        onAccepted: {
            selectedColor.color = colorDialog.color;
        }
    }
}
